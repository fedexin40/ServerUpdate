<a name=""></a>
##  (2016-04-07)


#### Features

* **config file:**  adding a config file to read the database specifications from here ([0a6a3246](http://gitlab.openpyme.mx/lazaro/update_server.git/commit/0a6a3246c5bc89636e1ffc022d2d652bc9d5d05a))
* **update servers:**  add the script to update servers ([1656417b](http://gitlab.openpyme.mx/lazaro/update_server.git/commit/1656417bb479eb1a783d398e201331a1e53c7985))
* **update_server:**  add the first version to update_server file ([930bbb27](http://gitlab.openpyme.mx/lazaro/update_server.git/commit/930bbb274bcbf06795bd266aec39884fd1150e40))

#### Performance

* **update servers:**  change the queries to improve the performance ([437434d9](http://gitlab.openpyme.mx/lazaro/update_server.git/commit/437434d99e95d34412da83782101f90706bf22ae))



