# -*- coding: utf-8 -*-
"""
Scrpit uses to update the data from localhost to another remote server
To update the values compares the write_date(timestamp field) with other field name
write_date_remote(timestamp too)
"""
import ConfigParser
from sqlalchemy import create_engine
from sqlalchemy.sql import select
from sqlalchemy.sql import table
from sqlalchemy.sql import column
from sqlalchemy.sql import insert
from sqlalchemy import INTEGER
from sqlalchemy import VARCHAR
from sqlalchemy import TIMESTAMP
from sqlalchemy import DATETIME
import datetime

config = ConfigParser.ConfigParser()
config.readfp(open('config/config.init'))

# Get remote server values from config.init
remote_dialect = config.get('remote_server', 'dialect')
remote_username = config.get('remote_server', 'username')
remote_password = config.get('remote_server', 'password')
remote_host = config.get('remote_server', 'host')
remote_port = config.get('remote_server', 'port')
remote_database = config.get('remote_server', 'database')

# Get local server values from config.init
local_dialect = config.get('local_server', 'dialect')
local_username = config.get('local_server', 'username')
local_password = config.get('local_server', 'password')
local_host = config.get('local_server', 'host')
local_port = config.get('local_server', 'port')
local_database = config.get('local_server', 'database')

# Currency date
currency_date = datetime.datetime.now()

# Create remote engine with the values read in the config file
if remote_password:
    string_remote = ''.join(
        [remote_dialect, '://', remote_username, ':', remote_password,
         '@', remote_host, ':', remote_port, '/', remote_database]
    )
else:      
    string_remote = ''.join(
        [remote_dialect, ':///', remote_database]
    )
remote_engine = create_engine(string_remote, echo=True)

# Create local engine with the values read in the config file
if local_password:
    string_local = ''.join(
        [local_dialect, '://', local_username, ':', local_password,
         '@', local_host, ':', local_port, '/', local_database]
    )
else:      
    string_local = ''.join(
        [local_dialect, ':///', local_database]
    )
local_engine = create_engine(string_local, echo=True)

# Create a model from the table to copy
cat_estructura = table(
    'cat_estructura',
    column('ID_ESTRUCTURA', INTEGER),
    column('CLAVE_ELECTOR', VARCHAR),
    column('NOMBRE', VARCHAR),
    column('APELLIDO_PATERNO', VARCHAR),
    column('APELLIDO_MATERNO', VARCHAR),
    column('DOMICILIO', VARCHAR),
    column('COLONIA',VARCHAR),
    column('CP',VARCHAR),
    column('SECCION', VARCHAR),
    column('MUNICIPIO', VARCHAR),
    column('TEL1', VARCHAR),
    column('TEL2', VARCHAR),
    column('TEL3', VARCHAR),
    column('EMAIL1', VARCHAR),
    column('EMAIL2', VARCHAR),
    column('CAPTURISTA', INTEGER),
    column('DEPENDE', VARCHAR),
    column('DEP_REG',VARCHAR),
    column('DF',VARCHAR),
    column('DEP_ZONA', VARCHAR),
    column('DEP_SEC', VARCHAR),
    column('DEP_COP', VARCHAR),
    column('DEP_PRO', VARCHAR),
    column('CARGO', VARCHAR),
    column('FECHA_CAPTURA', TIMESTAMP),
    column('VALIDA', INTEGER),
    column('VALIDA_OBSERVA', VARCHAR),
    column('FECHA_VAL', DATETIME),
    column('CAPTURISTA_VAL',INTEGER),
    column('VAL_DEP',INTEGER),
    column('VAL_CAR', INTEGER),
    column('LLAMADAS', INTEGER),
    column('PREFIJO', VARCHAR),
    column('REF_C', INTEGER),
    column('FOTO', VARCHAR),
    column('LN', INTEGER),
    column('ID_RED', INTEGER),
    column('ESTATUS', INTEGER),
    column('ASIGNACION', VARCHAR),
    column('P1',VARCHAR),
    column('FILTRO',VARCHAR),
    column('PROCESO', INTEGER),
    column('LLAMAR_MT', DATETIME),
    column('PROCESO_TM', DATETIME),
    column('ELECTORAL', VARCHAR),
    column('CORTE',INTEGER),
    column('ID_EVENTO',INTEGER),
    column('X', VARCHAR),
    column('Y', VARCHAR),
    column('RED_EVENTO', VARCHAR),
    column('FACEBOOK', VARCHAR),
    column('TWITTER', VARCHAR),
    column('TEL1_R', INTEGER),
    column('TEL2_R', INTEGER),
    column('DL', VARCHAR),
    column('INE_FRENTE', VARCHAR),
    column('INE_REVERSO',VARCHAR),
    column('ID_RED_COM',INTEGER),
    column('CARGO2', VARCHAR),
    column('TIPO', VARCHAR),
    column('ID_REPLICA', INTEGER),
    column('FECHA_REPLICA', DATETIME),
    column('PERTENENCIA', VARCHAR),
)

# Open connection in the local server
with local_engine.begin() as connection_local:
    # Select new rows to add
    query = select(['*']).where(
        cat_estructura.c.FECHA_REPLICA == None
    )
    new_values = connection_local.execute(query)
    
    # Select rows to only update
    query = select(['*']).where(
        cat_estructura.c.FECHA_CAPTURA > cat_estructura.c.FECHA_REPLICA
    )
    update_values = connection_local.execute(query)
    
    query_new_values = []
    query_update_values = []
    ids_news = {}
    ids_update = {}
    # Convert proxy_result into a dictionary
    for new_value in new_values:
        query_new_values.append(dict(zip(
            new_value.keys(), new_value.values()
        )))
    
    # Convert proxy_result into a dictionary
    for update_value in update_values:
        query_update_values.append(dict(zip(
            update_value.keys(), update_value.values()
        )))
    
    # Open connection in the remote server
    with remote_engine.begin() as connection_remote:
        # Insert values into the remote server
        # Copy the new id into ids dictionay to after update into local server
        for query_new_value in query_new_values:
            # Crate a dictionary with ids as key and None as value
            # and create one for each register to insert
            ids_news.update({query_new_value['ID_ESTRUCTURA']: None})
            id = query_new_value['ID_ESTRUCTURA']
            # Detele the ID_ESTRUCTURA from the dictionay values  because
            # sqlalchemy will try to insert the new register
            # with ID_ESTRUCTURA equal to the given ID_ESTRUCTURA
            # in the dictionary
            del query_new_value['ID_ESTRUCTURA']
            # This filds do not exist in remote database
            del query_new_value['ID_REPLICA', 'FECHA_REPLICA']
            # Insert the new register and return the new id
            query_insert = cat_estructura.insert().values(
                [query_new_value]
            ).returning(cat_estructura.c.ID_ESTRUCTURA)
            new_id = connection_remote.execute(query_insert)
            # Update the ids dictionary with
            # the returned id in the insert query
            ids_news[id] = new_id.fetchone()[0]
        # Updtes values into remote server
        for query_update_value in query_update_values:
            # Copy ID_ESTRUCTURA inside a list
            ids_update.update(query_update_value['ID_ESTRUCTURA'])
            id = query_update_value['ID_ESTRUCTURA']
            # This filds do not exist in remote database
            # or are not required as ID_ESTRUCTURA
            del query_new_value[
                'ID_REPLICA', 'FECHA_REPLICA', 'ID_ESTRUCTURA'
            ]
            query_update = cat_estructura.update().where(
                cat_estructura.c.ID_ESTRUCTURA == id
            ).values(query_update_value)
            connection_remote.execute(query_update)
    # Update with id remote and write_date_remote
    # in the local table; only for the new values
    for id_local, id_remote in ids_news.items():
        query_update = cat_estructura.update().where(
            cat_estructura.c.id == id_local
        ).values(
            id_remote=id_remote, write_date_remote=currency_date,
        )
        connection_local.execute(query_update)
    # Update with id remote and write_date_remote the other values
    query_update = cat_estructura.update().where(
        cat_estructura.c.ID_ESTRUCTURA.in_(ids_update)
    ).values(
        write_date_remote=currency_date,
    )
    connection_local.execute(query_update)
    