# -*- coding: utf-8 -*-
"""
This script reads a csv file given within the same script
and load the information in a mysql database
"""
import csv
from sqlalchemy import INTEGER
from sqlalchemy import VARCHAR
from sqlalchemy import create_engine
from sqlalchemy.sql import table
from sqlalchemy.sql import column

# Create engine
engine = create_engine(
    'mysql://root:10140089@localhost/lazaro', echo=True,
)

# Create the table model
cat_lista_cp = table(
    'cat_lista_cp',
    column('id_contacto', INTEGER),
    column('entidad',VARCHAR),
    column('municipio',VARCHAR),
    column('seccion',VARCHAR),
    column('localidad',VARCHAR),
    column('manzana', INTEGER),
    column('id_persona', INTEGER),
    column('clave_elector', VARCHAR),
    column('apellido_paterno', VARCHAR),
    column('apellido_materno', VARCHAR),
    column('nombre', VARCHAR),
    column('fecha_nacimiento', VARCHAR),
    column('lugar_nacimiento', VARCHAR),
    column('sexo', VARCHAR),
    column('calle', VARCHAR),
    column('num_exterior', VARCHAR),
    column('num_interior', VARCHAR),
    column('colonia', VARCHAR),
    column('codigo_postal', VARCHAR),
    column('tiempo_residencia', INTEGER),
    column('_3x1', INTEGER),
    column('fonart', INTEGER),
    column('opo', INTEGER),
    column('paja', INTEGER),
    column('pal', INTEGER),
    column('pam', INTEGER),
    column('pasl', INTEGER),
    column('pdzp', INTEGER),
    column('pei', INTEGER),
    column('_3x1', INTEGER),
    column('pet', INTEGER),
    column('pop', INTEGER),
    column('sevije', INTEGER),
)

# Read csv file and create a list of dictionaries with the values
with open('contactos.csv', 'rt') as csvfile:
    file = csv.reader(csvfile, delimiter='|')
    
    # Open connection in the remote server
    with engine.begin() as connection:
        for index, row in enumerate(file):
            if index == 0:
                headers = row
            else:
                dictionary = dict(zip(headers, row))
                # Insert values inside table
                query_insert = cat_lista_cp.insert().values([dictionary])
                connection.execute(query_insert)
