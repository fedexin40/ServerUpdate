# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='ServerUpdate',
    version='1.0.0',
    description='Tool to update server',
    author='Federico Curz',
    author_email='federico.cruz@openpyme.mx',
    license='MIT',
    packages=find_packages(),
    scripts=[
        'update_server'
    ],
    url='http://openpyme.mx',
    install_requires=[
        'sqlalchemy',
        'configparser',
    ],
)
